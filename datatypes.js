
var x="25";
console.log(typeof x);  //string

var x=["html","css"]
console.log(typeof x);   //it's an array but op will be object

var x={name:"Nirali Vaghela"}
console.log(typeof x);  //object 

var x=25;
console.log(typeof x); //number

var x=true
console.log(typeof x);  //boolean

var x;
console.log(typeof x); //undefined

var x=null;
console.log(typeof x); //object
