// function declaration
function calculatePer(price, percentage) {
   
    return (percentage/100) * price;
  }
  var ans=calculatePer(2000,20);
  console.log(ans);
  // function expression
  var divideByN = function(number, n) {
    return number/n;
  }
  
  var answer=divideByN(1000,10)
  console.log(answer);


  // anonymous function
  (function() {
    console.log('I am an anonymous function expression');
  }());
//   Hoisting is JavaScript's default behavior of moving declarations to the top of the current scope.

//   Hoisting applies to variable declarations and to function declarations.
  
//   Because of this, JavaScript functions can be called before they are declared:

myFunction(5);

function myFunction(y) {
  return y * y;
}

// ES5
var x = function(x, y) {
    return x * y;
  }
  
  // ES6
//   const x = (x, y) => x * y;  //arrow functions
  function findMax() {
    let max = -Infinity;
    for(let i = 0; i < arguments.length; i++) {
      if (arguments[i] > max) {
        max = arguments[i];
      }
    }
    return max;
  } 
  let op= findMax(4, 5, 6);
  console.log(op);


  
  const person = {
    fullName: function(city, country) {
      return this.firstName + " " + this.lastName + "," + city + "," + country;
    }
  }
  
  const person1 = {
    firstName:"John",
    lastName: "Doe"
  }
  
  console.log(person.fullName.call(person1, "Oslo", "Norway"));


