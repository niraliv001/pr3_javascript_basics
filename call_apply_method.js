//   The call() method takes arguments separately.

// The apply() method takes arguments as an array.

const person = {
    fullName: function(city, country) {
      return this.firstName + " " + this.lastName + "," + city + "," + country;
    }
  }
  
  const person1 = {
    firstName:"N",
    lastName: "V"
  }
  
  console.log( person.fullName.call(person1, "abad", "India"));//N V,abad,India


  //apply exp here..
  const personal = {
    fullName: function(city, country) {
      return this.firstName + " " + this.lastName + "," + city + "," + country;
    }
  }



  const person2 = {
    firstName:"Abc",
    lastName: "Xyz"
  }
  
  console.log( personal.fullName.apply(person2, ["abad", "India"]));  //Abc Xyz,abad,India