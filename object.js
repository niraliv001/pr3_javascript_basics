const person = {
    name: 'Nirali',
    age: 20
  };
  
  // name and age are properties of the one object.
  
  // dot notation.
  
  console.log(person.age);   //20
  // bracket notation.
  // This is useful if you're accessing a property through a variable
 console.log(person["name"]);    //Nirali
  